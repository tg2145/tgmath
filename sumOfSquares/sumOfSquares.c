#include <sumOfSquares.h>

double sumOfSquares(double* data, int length)
{
  double sumOfSquares = 0;
  for(int n=0;n<length;++n)
  {
    sumOfSquares+=data[n]*data[n];
  }
  return sumOfSquares;
}