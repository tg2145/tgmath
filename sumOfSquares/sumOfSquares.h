#ifndef TGMATH_SUMOFSQUARES_H
#define TGMATH_SUMOFSQUARES_H

double sumOfSquares(double* data, int length);

#endif