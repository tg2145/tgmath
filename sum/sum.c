#include <sum.h>

double sum(double* data, int length)
{
  double sum = 0;
  for(int n=0;n<length;++n)
  {
    sum+=data[n];
  }
  return sum;
}
