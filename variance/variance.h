#ifndef TGMATH_VARIANCE_H
#define TGMATH_VARIANCE_H

double variance(double* data, int length);
double varianceShortcut(double* data, int length);

#endif