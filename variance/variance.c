#include <variance.h>

double variance(double* data, int length)
{
  double sum = 0;
  for(int n=0;n<length;++n)
  {
    sum += data[n];
  }
  double average = sum/length;
  double varianceSum = 0;
  for(int n=0;n<length;++n)
  {
    varianceSum += (average - data[n]) * (average - data[n]);
  }
  return varianceSum/(length-1);
}

double varianceShortcut(double* data, int length)
{
  double dataSum = 0;
  double squareDataSum = 0;
  for(int n=0;n<length;++n)
  {
    dataSum += data[n];
    squareDataSum += data[n] * data[n];
  }
  return ((length * squareDataSum) - (dataSum * dataSum)) / (length * (length - 1));
}