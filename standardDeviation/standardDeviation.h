#ifndef TGMATH_STANDARDDEVIATION_H
#define TGMATH_STANDARDDEVIATION_H

double standardDeviation(double* data, int length);

#endif