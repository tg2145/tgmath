#include <standardDeviation.h>
#include <math.h>

double standardDeviation(double* data, int length)
{
  double sum = 0;
  for(int n=0;n<length;++n)
  {
    sum += data[n];
  }
  double average = sum/length;
  double varianceSum = 0;
  for(int n=0;n<length;++n)
  {
    varianceSum += (average - data[n]) * (average - data[n]);
  }
  return sqrt(varianceSum/(length-1));
}