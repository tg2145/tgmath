#include <mean.h>

double mean(double* data, int length)
{
  double sum = 0;
  for(int n=0;n<length;++n)
  {
    sum+=data[n];
  }
  return sum/length;
}